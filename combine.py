import os

files = list(filter(lambda x: x.endswith("result.csv") and x != "result.csv", next(os.walk("."))[2]))

input = []

for file in files:
	with open(file, "r") as f:
		read = list(map(lambda x: x.strip().split(","), f.readlines()))
		input.append(read)

runs = len(input[0])
processes = len(input)

result = [[] for x in range(runs)]

for i in range(runs):
	for j in range(processes):
		result[i].extend(input[j][i])

"""
for i in range(len(input)):
	print(input[i])

print("")

for i in range(len(result)):
	print(result[i])
"""

with open("result.csv", "w") as f:
	for i in range(len(result)):
		f.write(",".join(filter(lambda x: x != "", result[i])))
		f.write("\n")
