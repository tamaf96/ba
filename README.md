Das Programm Weltraumschrott.c dient als reiner proof of concept und ist nicht frei von Fehlern.
Zum Bauen und ausführen wird folgendes benötigt:
 - MPI/MPICH
 - ffmpeg
 - python 3

 Zum Bauen und Ausführen gibt es folgendes Script in 2 Versionen:
 run.sh (ohne Paramater)
 run.sh --debug (löscht die Zwischendateien nicht)

 Das Script ruft die Folgenden Befehle nacheinander auf:

Zum compilieren:
`mpicc -lm -o weltraum weltraumschrottmodell.c`

Zum ausführen lokal:
`mpirun -np 3 ./weltraum`

Output zusammenbasteln:
`python combine.py`

Visualisierung:
`python visualization.py`

Video erstellen:
`ffmpeg -r 60 -f image2 -s 1920x1080 -i picture%d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p -loglevel error video.mp4`

Alle Zwischenprodukte löschen:
`rm picture*.png`
`rm *result.csv`


Das Programm selbst nimmt keine Parameter entgegen.
Man kann die folgenden Stellschrauben in der c-Datei verändern:

MAX_X: Maximale Breite der Fläche
MAX_Y: Maximale Höhe der Fläche
SPAWN: Anzahl neuer Objekte bei Kollision
t: Anzahl berechneter Zeiteinheiten

Folgende Stellschrauben müssen in der input.csv Datei angepasst werden:

Anzahl der Startobjekte und deren Eigenschaften.
Pro Zeile ein Objekt, lies: Koord_x, koord_y, mass, richtung_x, richtung_y

