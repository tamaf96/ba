#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>

const int MAX_X = 224;
const int MAX_Y = 224;
const int SPAWN = 5;
const double PI = 3.14159;

///////////// Structs ///////////////////////

struct vector {
    double x;
    double y;
};

struct obj {
    double koord_x;
    double koord_y;
    int mass;
    struct vector direction;
    struct vector temp_vec;
};

struct structural_info {
    int subdivisions_x;
    int subdivisions_y;
};


//Funktionen
void remove0fromArray(int primefactor[], int primefactor_count);
struct structural_info GetInfo(int size);
int maxVal(int a, int b);
int getIndex(double koord_x,double koord_y,struct structural_info info);
double distance(struct obj a, struct obj b);
struct vector normalize(struct vector v);
struct obj generateObj(struct obj a, struct obj b, int index );
double gravity(struct obj a, struct obj b);
struct vector calc_direction(struct obj a, struct obj b);


int main(int argc, char *argv[]) {
    int rank, size;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // create MPI datatype for vector
    MPI_Datatype MPI_VECTOR;
    // set up variables
    int count = 2;
    int array_of_blocklengths[2] = {1, 1};
    MPI_Aint array_of_displacements[2] = {offsetof(struct vector, x), offsetof(struct vector, y)};
    MPI_Datatype array_of_types[2] = {MPI_DOUBLE, MPI_DOUBLE};
    // create and commit new MPI datatype
    MPI_Type_create_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, &MPI_VECTOR);
    MPI_Type_commit(&MPI_VECTOR);
    
    // create MPI datatype for Object
    MPI_Datatype MPI_OBJ;
    // set up variables
    count = 5;
    int array_of_blocklengths2[5] = {1, 1, 1, 1, 1};
    MPI_Aint array_of_displacements2[5] = {offsetof(struct obj, koord_x), offsetof(struct obj, koord_y), offsetof(struct obj, mass), offsetof(struct obj, direction), offsetof(struct obj, temp_vec)};
    MPI_Datatype array_of_types2[5] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT, MPI_VECTOR, MPI_VECTOR};
    // create and commit new MPI datatype
    MPI_Type_create_struct(count, array_of_blocklengths2, array_of_displacements2, array_of_types2, &MPI_OBJ);
    MPI_Type_commit(&MPI_OBJ);

    //Anzahl Objekte
    int n = 0; 
    int obj_count = 0;
    struct obj *objects;

    //Name für result Datei zusammenbasteln
    char str[13];
    sprintf(str, "%d", rank);
    strcat(str, "result.csv");
    FILE *fpr = fopen(str, "w");

    //Informationen über die Fläche und eigene Position berechnen
    struct structural_info info = GetInfo(size);
    if(rank == 0){
        //Zaehlen, wie viele Objekte am Anfang da sind und in Variable n speichern
        char ch;
        FILE *fp = NULL;
        fp = fopen ("input.csv", "r");
        if (NULL == fp) {
            printf("file can't be opened \n");
        }
        while (!feof(fp)) {
            ch = fgetc(fp);
            if(ch == '\n'){
                n++;
            }
        }
        fclose(fp);
        n++; //fuer den fall, dass in der letzten zeile ein \n fehlt.

        //den anderen Prozessen mitteilen, wie viele Bodys es insgesamt gibt
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

        //array anlegen fuer objekte. (hier waere ein dynamisches array eigentlich das richtige, aber da es nur ein prototy ist, mach ich einfach ein grosses array und hoffe, dass es beim test nicht ueberlauft)
        objects = malloc(5*n * sizeof(struct obj));
        

        //temporaeres objekt anlegen zum einlesen der daten
        struct obj temp_obj;

        //Datein einlesen und verteilen
        fp = fopen ("input.csv", "r");
        while (!feof(fp)) {
            fscanf(fp, "%lf,%lf,%d,%lf,%lf\n", &temp_obj.koord_x, &temp_obj.koord_y, &temp_obj.mass, &temp_obj.direction.x, &temp_obj.direction.y);
            int index = getIndex(temp_obj.koord_x,temp_obj.koord_y,info);
            if (rank == index){ // objekt gehoert zu prozess 0
                objects[obj_count].koord_x = temp_obj.koord_x;
                objects[obj_count].koord_y = temp_obj.koord_y;
                objects[obj_count].mass = temp_obj.mass;
                objects[obj_count].direction.x = temp_obj.direction.x;
                objects[obj_count].direction.y = temp_obj.direction.y;
                objects[obj_count].temp_vec.x = 0;
                objects[obj_count].temp_vec.y = 0;
                obj_count++;
            }
            else{//objekt gehoert zu einem anderen prozess
                MPI_Send(&temp_obj, 1, MPI_OBJ, index, 0, MPI_COMM_WORLD);
            }
        }
        fclose(fp);

        //signal, dass alle objekte verteilt wurden
        struct obj signal_obj;
        signal_obj.mass = 0;
        for (int j = 0; j < size; j++){
            MPI_Send(&signal_obj, 1, MPI_OBJ, j, 0, MPI_COMM_WORLD);
        }

    }
    else{
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
        
        objects = malloc(50*5*n * sizeof(struct obj));
        bool beenden = false;

        struct obj new_obj;
        while(!beenden){
            MPI_Recv(&new_obj, 1, MPI_OBJ, 0,0, MPI_COMM_WORLD, &status);
            if (new_obj.mass == 0){
                beenden = true;
                break;
            }
            objects[obj_count] = new_obj;
            objects[obj_count].temp_vec.x = 0;
            objects[obj_count].temp_vec.y = 0;
            obj_count++;
        }
    }

    for(int t = 0; t < 250; t++)
    {
        //kollisionen finden und behandeln
        struct obj generateObjA[500];
        int generateObj_counter = 0;
        int deleteObjA[obj_count];
        int deleteObj_counter = 0;
        bool collision = false;
        
        for(int i = 0; i < obj_count; i++){
            for(int j = i; j < obj_count; j++){
                if (i == j){
                    continue;
                }
                if(distance(objects[i],objects[j]) < 1){
                    collision = true;
                    deleteObjA[deleteObj_counter] = i;
                    deleteObj_counter++;
                    deleteObjA[deleteObj_counter] = j;
                    deleteObj_counter++;
                    for(int k = 0; k < SPAWN; k++){
                        generateObjA[generateObj_counter] = generateObj(objects[i],objects[j],k);
                        generateObj_counter++;
                    }
                }
            }
        }
        if (collision = true){
            for(int i = 0; i < deleteObj_counter; i++){
                objects[deleteObjA[i]] = objects[obj_count - 1];
                obj_count--;
        
            }
            for(int i = 0; i < generateObj_counter; i++){
                objects[obj_count] = generateObjA[i];
                obj_count++;
            
            }
        }
        
        //berechnung Gravitation im eigenen Rechteck
        for(int i = 0; i < obj_count; i++){
            for(int j = 0; j < obj_count; j++){
                if(i == j){
                    continue;
                }
                double g = gravity(objects[i], objects[j]);
                struct vector w = calc_direction(objects[i], objects[j]);
                objects[i].temp_vec.x += w.x * g;
                objects[i].temp_vec.y += w.y * g;
                
            }
        }
       
        //Masseschwerpunkt berechnen
        struct obj masseschwerpunkt;
        int gesamtmasse = 0;
        int gesamt_x = 0;
        int gesamt_y = 0;
        if(obj_count > 0){
            for(int i = 0; i < obj_count; i++){
                gesamtmasse += objects[i].mass;
                gesamt_x += objects[i].koord_x * objects[i].mass;
                gesamt_y += objects[i].koord_y * objects[i].mass;
            }
            masseschwerpunkt.koord_x = gesamt_x / gesamtmasse;
            masseschwerpunkt.koord_y = gesamt_y / gesamtmasse;
            masseschwerpunkt.mass = gesamtmasse / obj_count;
        }
        else{
            masseschwerpunkt.koord_x = 0;
            masseschwerpunkt.koord_y = 0;
            masseschwerpunkt.mass = 0;
        }

        //Masseschwerpunkt übertragen
        MPI_Request myRequest;
        MPI_Request myRequest2;
        
        for(int i = 0; i < size; i++){
            if (rank == i){
                continue;
            }
            MPI_Isend(&masseschwerpunkt, 1, MPI_OBJ, i, 1, MPI_COMM_WORLD, &myRequest);
            MPI_Wait(&myRequest, &status);
        }
        //Masseschwerpunkte einpflegen
        struct obj massepunkteA[size];
        MPI_Request requestA[size];
        int request_counter = 0;
        struct obj newMasse;
        int ind;
        for(int i = 0; i < size; i++){
            if (rank == i){
                continue;
            }
            
            MPI_Irecv(&newMasse, 1, MPI_OBJ, i,1, MPI_COMM_WORLD, &myRequest2);
            MPI_Wait(&myRequest, &status);
            
            MPI_Wait(&myRequest2, &status);
            
            for(int j = 0; j < obj_count; j++){
                double g = gravity(objects[j], newMasse);
                struct vector w = calc_direction(objects[j], newMasse);
                objects[j].temp_vec.x += w.x * g;
                objects[j].temp_vec.y += w.y * g;

            }
        }
        
        //neue position berechnen
        for(int i = 0; i < obj_count; i++){
            
            objects[i].direction.x += objects[i].temp_vec.x;
            objects[i].direction.y += objects[i].temp_vec.y;
            objects[i].temp_vec.x = 0;
            objects[i].temp_vec.y = 0;
            objects[i].koord_x += objects[i].direction.x;
            objects[i].koord_y += objects[i].direction.y;
            if (objects[i].koord_x < 0){
                objects[i].koord_x = MAX_X + objects[i].koord_x;
            }
            if(objects[i].koord_x > MAX_X){
                objects[i].koord_x = objects[i].koord_x - MAX_X ;
            }
            if (objects[i].koord_y < 0){
                objects[i].koord_y = MAX_Y + objects[i].koord_y;
            }
            if(objects[i].koord_y > MAX_Y){
                objects[i].koord_y = objects[i].koord_y - MAX_Y ;
            }
        }

        
        //result csv erstellen
        for(int i = 0; i < obj_count; i++){
            if (i == 0){
                fprintf(fpr, "%d.%d",(int) objects[i].koord_x, (int) objects[i].koord_y);
            } else{
                fprintf(fpr, ",%d.%d",(int) objects[i].koord_x, (int) objects[i].koord_y);
            }
        }
        fprintf(fpr, "\n");
        
        
        //Uebertragen der ueber die Grenze gekommenen Objekte
        deleteObj_counter = 0;
        int sendArray[size];
        for(int i = 0; i < size; i++){
            sendArray[i] = 0;
        }
        for(int i = 0; i < obj_count; i++){
            int index = getIndex(objects[i].koord_x,objects[i].koord_y,info);
            if (index != rank){
                sendArray[index] += 1;
                deleteObjA[deleteObj_counter] = i;
                deleteObj_counter++;
            }
        }
        
        int recvArray[size];
        int recvArray_temp[size];
        for(int i = 0; i < size; i++){
            recvArray[i] = 0;
        }

        for(int i = 0; i < size; i++){
            if (rank == i){
                continue;
            }
            MPI_Isend(sendArray, size, MPI_INT, i, 0, MPI_COMM_WORLD, &myRequest);
            MPI_Wait(&myRequest, &status);
            
        }
        
        for(int i = 0; i < size; i++){
            if (rank == i){
                continue;
            }
            
            MPI_Irecv(recvArray_temp, size, MPI_INT, i,0, MPI_COMM_WORLD, &myRequest);
            MPI_Wait(&myRequest,&status);
            
            recvArray[i] = recvArray_temp[rank]; 
        }
        for(int i = 0; i < obj_count; i++){
            int index = getIndex(objects[i].koord_x,objects[i].koord_y,info);
            if (index != rank){
                MPI_Isend(&objects[i], 1, MPI_OBJ, index, 1, MPI_COMM_WORLD, &myRequest);
                MPI_Wait(&myRequest, &status);
            }
        }
        
        struct obj new;
        generateObj_counter = 0;
        for(int i = 0; i < size; i++){
            if (recvArray[i] == 0){
                continue;
            }
            for(int j = 0; j < recvArray[i]; j++){
                MPI_Irecv(&new,1,MPI_OBJ,i,1,MPI_COMM_WORLD, &myRequest);
                MPI_Wait(&myRequest, &status);
                generateObjA[generateObj_counter] = new;
                generateObj_counter++;
            }
        }
        for(int i = 0; i < deleteObj_counter; i++){
                objects[deleteObjA[i]] = objects[obj_count - 1];
                obj_count--;       
        }
        for(int i = 0; i < generateObj_counter; i++){
                objects[obj_count] = generateObjA[i];
                obj_count++; 
        }
        MPI_Barrier;
        
    }

    fclose(fpr);
    free(objects);
    MPI_Finalize();
    return(0);
} 

void remove0fromArray(int primefactor[], int primefactor_count){
    for(int i; i < primefactor_count-1; i++){
        if (primefactor[i] == 0){
            for (int j = i; j< primefactor_count-1; j++){
                primefactor[j] = primefactor[j+1];
            }
        }
    }
}

struct structural_info GetInfo(int size){
    struct structural_info info;
    int primefactor[10];
    int primefactor_count = 0;
    bool found = false;
    int p;
    int s = size;
    while (s > 1){
        int i = 2;
        found = false;
        while(!found && (i*i <= s)){
            if (s % i == 0){
                found = true;
                p = i;
            }
            else{
                i = i + 1;
            }
        }
        if (!found){
            p = s;
        }
        primefactor[primefactor_count] = p;
        primefactor_count++;
        s = s / p;
    }
    
    if (primefactor_count == 1){
        primefactor[1] = 1;
    }
    while(primefactor_count > 2){
        if (primefactor[0] > primefactor[primefactor_count-1]){
            primefactor[primefactor_count-2] = primefactor[primefactor_count-2] * primefactor[primefactor_count-1];
            primefactor_count--;
        }
        else{
             primefactor[0] = primefactor[0] * primefactor[1];
            primefactor[1] = 0;
            remove0fromArray(primefactor, primefactor_count);
            primefactor_count--;
        }
    }
    info.subdivisions_x = primefactor[0];
    info.subdivisions_y = primefactor[1];
    return(info);
}

int maxVal(int a, int b){
    if (a < b){
        return b;
    }
    return a;
}
    
int getIndex(double koord_x,double koord_y,struct structural_info info){
    double section_width = MAX_X / (double)info.subdivisions_x;
    double section_height = MAX_Y / (double)info.subdivisions_y;
    int x_index = maxVal(0, ceil(koord_x/section_width)-1);
    int y_index = maxVal(0, ceil(koord_y/section_height)-1);
    return (y_index * info.subdivisions_x + x_index);
}

double distance(struct obj a, struct obj b) {
    return sqrt(pow(a.koord_x - b.koord_x, 2) + pow(a.koord_y - b.koord_y, 2));
}

struct vector normalize(struct vector v) {
    // normalize given vector
    double abs = sqrt(pow(v.x, 2) + pow(v.y, 2));
    if(abs != 0){
        v.x = v.x / abs;
        v.y = v.y / abs;
    }
    return v;
}


struct obj generateObj(struct obj a, struct obj b, int index ){
    struct obj newObj;
    //neue richtung
    struct vector vekt;
    vekt.x = 0;
    vekt.y = 1;

    int drehung_deg = (360/SPAWN) * index;
   
    double drehung_rad = (double) drehung_deg * (PI / 180.0);
   
    newObj.direction.x = -sin(drehung_rad);
    newObj.direction.y = cos(drehung_rad);

    //neue koordinaten
    newObj.koord_x = a.koord_x + (10 * newObj.direction.x);
    newObj.koord_y = a.koord_y + (10 * newObj.direction.y);

    //neue masse
    int total_mass = a.mass + b.mass;
    int new_mass = total_mass / SPAWN;
    newObj.mass = new_mass;

    //temporaerer vektor
    newObj.temp_vec.x = 0;
    newObj.temp_vec.y = 0;

    
    return(newObj);
}

double gravity(struct obj a, struct obj b) {
    // calculate gravity force between two points
    const double G = 6674e-18;
    double grav;
    if (distance(a,b) != 0){
        grav = ((G * a.mass * b.mass) / distance(a, b));
    }
    else{
        grav = 0;
    }
    return(grav);
}


struct vector calc_direction(struct obj a, struct obj b) {
    // compute normalized vector from a to b
    struct vector v;
    v.x = b.koord_x - a.koord_x;
    v.y = b.koord_y - a.koord_y;
    return normalize(v);
}




