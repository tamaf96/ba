import png

BLACK  = (0,   0, 0)
COLOR1 = (50,  0, 0)
COLOR2 = (100, 0, 0)
COLOR3 = (150, 0, 0)
COLOR4 = (200, 0, 0)
COLOR5 = (255, 0, 0)
PALETTE = [BLACK, COLOR1, COLOR2, COLOR3, COLOR4, COLOR5]

WIDTH = 224
HEIGHT = 224

def create_image(lines, name):
    # initialize matrix with zeroes
    s = []
    for i in range (HEIGHT):
        si = []
        for j in range (WIDTH):
            si.append(0)
        s.append(si) 

    for i, line in enumerate(lines):
        coordinates = line.split(",")
        for point in coordinates:
            x, y = map(int, point.strip().split("."))
            s[x][y] = i
    
    w = png.Writer(WIDTH, HEIGHT, palette=PALETTE)
    with open(name + ".png", 'wb') as f:
        w.write(f, s)
 
with open("result.csv", "r") as fcsv1:
    lines = ""
    line1 = ""
    line2 = ""
    line3 = ""
    line4 = ""
    line5 = ""
    for i, line in enumerate(fcsv1):
        print("\r",i,end="")
        if i == 0:
            line1 = line
            continue
        if i == 1:
            line2 = line
            continue
        if i == 2:
            line3 = line
            continue
        if i == 3:
            line4 = line
            continue
        if i == 4:
            line5 = line
        if i > 4:
            line1 = line2
            line2 = line3
            line3 = line4
            line4 = line5
            line5 = line
        lines = [line1, line2, line3, line4, line5]
        name = "picture" + str(i)
        create_image(lines,name)
    print("")
