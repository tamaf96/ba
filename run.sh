#!/bin/bash

mpicc -lm -o weltraum weltraumschrottmodell.c
mpirun -np 3 ./weltraum
python combine.py
python visualization.py
ffmpeg -r 60 -f image2 -s 1920x1080 -i picture%d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p -loglevel error video.mp4
rm picture*.png
if [[ $* != *--debug* ]]; then
    rm *result.csv
fi
